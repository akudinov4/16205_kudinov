#include <stdexcept>
#include "bitset.h"

BitArray::BitArray() :
    bits(0),
    numOfBits(0),
    countTrue(0)
    {};

BitArray::~BitArray() = default;

BitArray::BitArray(int num_bits, unsigned long value) :
    bits(num_bits / LONGBITS + 1),
    numOfBits(num_bits),
    countTrue(0)
{

    if (num_bits < LONGBITS)
        value <<= (LONGBITS - num_bits);

    if (value){
        bits[0] = value;
        while(value){
            if(value % 2) countTrue++;
            value >>= 1;
       }
    }
}

BitArray::BitArray(const BitArray& b){
    *this = b;
}

void BitArray::swap(BitArray& b){
    this->bits.swap(b.bits);
    std::swap(this->countTrue, b.countTrue);
    std::swap(this->numOfBits, b.numOfBits);
}

BitArray& BitArray::operator=(const BitArray& b){
    bits = b.bits;
    numOfBits = b.numOfBits;
    countTrue = b.countTrue;
    return *this;
}

void BitArray::resize(int num_bits, bool value){
    if(num_bits < 0) throw std::exception();

    if(num_bits > size()){
        bits.resize(num_bits / LONGBITS + 1);
        int prevNumOfBits = size();
        numOfBits = num_bits;

        for(int i = prevNumOfBits; i < size(); i++)
            set(i, value);

        if(value) countTrue += num_bits - size();
    } else {
        bits.resize(num_bits / LONGBITS + 1);

        for(int i = num_bits; i < size(); i++)
            set(i, false);

        numOfBits = num_bits;
    }
}

void BitArray::clear(){
    bits.clear();
    numOfBits = 0;
    countTrue = 0;
}

void BitArray::push_back(bool bit){
    resize(size() + 1, bit);
}

BitArray& BitArray::operator&=(const BitArray& b){
    if (size() != b.size()) throw std::exception();
    for(int i = 0; i < bits.size(); i++){
        bits[i] &= b.bits[i];
    }
    countTrue = 0;
    for(int i = 0; i < size(); i++){
        countTrue += ((*this)[i]);
    }
    return *this;
}

BitArray& BitArray::operator|=(const BitArray& b){
    if (size() != b.size()) throw std::exception();
    for(int i = 0; i < bits.size(); i++){
        bits[i] |= b.bits[i];
    }
    countTrue = 0;
    for(int i = 0; i < size(); i++){
        countTrue += ((*this)[i]);
    }
    return *this;
}

BitArray& BitArray::operator^=(const BitArray& b){
    if (size() != b.size()) throw std::exception();
    for(int i = 0; i < bits.size(); i++){
        bits[i] ^= b.bits[i];
    }
    countTrue = 0;
    for(int i = 0; i < size(); i++){
        countTrue += ((*this)[i]);
    }
    return *this;
}

BitArray& BitArray::operator<<=(int n){
    if(n < 0) throw std::exception();

    if(n > size()) reset();

    for(int i = n; i < size(); i++){
        set(i - n, (*this)[i]);
    }

    for(int i = size() - n; i < size(); i++){
        set(i, false);
    }

    return *this;
}

BitArray& BitArray::operator>>=(int n){
    if(n < 0) throw std::exception();

    if(n > size()) reset();

    for(int i = size() - 1; i >= n; i--)
        set(i, (*this)[i - n]);

    for(int i = 0; i < n; i++)
        set(i, false);

    return *this;
}

BitArray BitArray::operator>>(int n) const{
    BitArray tmp = BitArray(*this);
    tmp >>= n;
    return tmp;
}

BitArray BitArray::operator<<(int n) const{
    BitArray tmp = BitArray(*this);
    tmp <<= n;
    return tmp;
}

BitArray& BitArray::set(int n, bool val){
    if (!(*this)[n] && val == true) countTrue++;
    else if ((*this)[n] && val == false) countTrue--;

    long mask = long(1) << (LONGBITS - 1 - (n % LONGBITS));

    if (val) bits.at(n / LONGBITS) |= mask;
    else bits.at(n / LONGBITS) &= ~mask;
    
    return *this;   
}

BitArray& BitArray::set(){
    countTrue = size();
    for(int i = 0; i < bits.size(); i++)
        bits[i] = ~(long(0));
    return *this;
}

BitArray& BitArray::reset(int n){
    set(n, false);
    return *this;
}

BitArray& BitArray::reset(){
    countTrue = 0;
    for(int i = 0; i < bits.size(); i++)
        bits[i] = 0;
    return *this;
}

bool BitArray::any() const{
    return !!countTrue;
}

bool BitArray::none() const{
    return !countTrue;
}

BitArray BitArray::operator~() const{
    BitArray tmp = BitArray(*this);
    tmp.countTrue = tmp.size() - tmp.countTrue;
    for(int i = 0; i < tmp.bits.size(); i++)
        tmp.bits[i] = ~tmp.bits[i];
    return tmp;
}

int BitArray::count() const{
    return countTrue;
}

bool BitArray::operator[](int i) const{
    if (i < 0 || i >= numOfBits) throw std::exception();

    return bits.at(i / LONGBITS) >> (LONGBITS - 1 - (i % LONGBITS)) & 1;
}


int BitArray::size() const{
    return numOfBits;
}

bool BitArray::empty() const{
    return !numOfBits;
}

std::string BitArray::to_string() const{
    if(size() == 0) return "";
    std::ostringstream tmp;
    for(int i = 0; i < this->size(); i++){
        tmp << (*this)[i];
    }

    return tmp.str();
}

bool operator==(const BitArray & a, const BitArray & b){
    if (a.size() != b.size()) return false;

    bool result = true;
    for(int i = 0; i < a.size(); i++){
        if (a[i] != b[i]) result = false;
    }

    return result;
}

bool operator!=(const BitArray & a, const BitArray & b){
    return !(a == b);
}   

BitArray operator&(const BitArray & a, const BitArray & b){
    BitArray tmp = BitArray(a);
    tmp &= b;
    return tmp;
}

BitArray operator|(const BitArray & a, const BitArray & b){
    BitArray tmp = BitArray(a);
    tmp |= b;
    return tmp;
}

BitArray operator^(const BitArray & a, const BitArray & b){
    BitArray tmp = BitArray(a);
    tmp ^= b;
    return tmp;
}