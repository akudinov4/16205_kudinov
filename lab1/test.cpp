#include "bitset.h"
#include "gtest/gtest.h"

TEST(BitSetTest, Resize){
    BitArray a = BitArray();
    a.resize(10, false);
    a.set(0, true);
    a.set(6, true);
    a.resize(5);
    EXPECT_EQ("10000", a.to_string());
    EXPECT_EQ(1, a.count());
    EXPECT_EQ(5, a.size());
    
    EXPECT_ANY_THROW(a.resize(-5)); 
}

TEST(BitSetTest, Swap){
    BitArray a = BitArray(10, 31),
             a_tmp = BitArray(a),
             b = BitArray(11, 13),
             b_tmp = BitArray(b);
    a.swap(b);
    EXPECT_EQ(true, a == b_tmp);
    EXPECT_EQ(true, b == a_tmp);
}

TEST(BitSetTest, PushBack){
    BitArray a = BitArray(2, 0);
    std::string str = "00";
    int count = 0;
    for(int i = 0; i < 100; i++){
        a.push_back((bool)(i % 2));
        str += std::to_string(i % 2);
        count += i % 2;
        EXPECT_EQ(str, a.to_string());
        EXPECT_EQ(count, a.count());
        EXPECT_EQ(i + 3, a.size());
    }
}

TEST(BitSetTest, Bitwise){
    BitArray a = BitArray(100, 0);
    for(int i = 68; i < 78; i++) a.set(i, true);
    BitArray b = a << 68;
    EXPECT_EQ("1111111111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", b.to_string());
    EXPECT_EQ(10, b.count());
    b >>= 68;
    EXPECT_EQ(true, a == b) << "Bit_Massive a : " << a.to_string() << std::endl
                            << "Bit_Massive b : " << b.to_string() << std::endl;
    EXPECT_EQ(10, b.count());
    b <<= 69;
    EXPECT_EQ("1111111110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", b.to_string());  
    EXPECT_EQ(9, b.count());  
    b = b >> 69;
    a.reset(68);
    EXPECT_EQ(true, a == b);
    EXPECT_EQ(9, a.count());
    EXPECT_EQ(9, b.count());
    
    EXPECT_ANY_THROW(a << -1);
    EXPECT_ANY_THROW(a >> -1);
    EXPECT_ANY_THROW(b >>= -1);
    EXPECT_ANY_THROW(b <<= -1);
}

TEST(BitSetTest, ToString){
    BitArray a = BitArray(10, 0);
    a.set(0, true);
    EXPECT_EQ("1000000000", a.to_string());
    EXPECT_EQ(1, a.count());
    a.set(1, true);
    EXPECT_EQ("1100000000", a.to_string());
    EXPECT_EQ(2, a.count());
    a.set(2, true);
    EXPECT_EQ("1110000000", a.to_string());
    EXPECT_EQ(3, a.count());
    a.set(3, true);
    EXPECT_EQ("1111000000", a.to_string());
    EXPECT_EQ(4, a.count());
    a.set(4, true);
    EXPECT_EQ("1111100000", a.to_string());
    EXPECT_EQ(5, a.count());
    a.set(5, true);
    EXPECT_EQ("1111110000", a.to_string());
    EXPECT_EQ(6, a.count());
    a.set(6, true);
    EXPECT_EQ("1111111000", a.to_string());
    EXPECT_EQ(7, a.count());
    a.set(0, false);
    EXPECT_EQ("0111111000", a.to_string());
    EXPECT_EQ(6, a.count());
    a.reset(1);
    EXPECT_EQ("0011111000", a.to_string());
    EXPECT_EQ(5, a.count());
}

TEST(BitSetTest, Set){
    BitArray a = BitArray(100, 0);
    for(int i = 0; i < a.size(); i++){
        a.set(i, true);
        EXPECT_EQ(true, a[i]) << "EXPECT: True, REAL: " << a[i] << std::endl 
                              << "Iteration:" << i << std::endl
                              << "Bit Massive: " << a.to_string();
        EXPECT_EQ(i + 1, a.count());
        EXPECT_EQ(100, a.size());
    }
    
    EXPECT_ANY_THROW(a[-1]);
    EXPECT_ANY_THROW(a[100]);
}

TEST(BitSetTest, AnyNoneEmpty){
    BitArray a = BitArray(100, 0);
    EXPECT_EQ(false, a.any());
    EXPECT_EQ(true, a.none());
    EXPECT_EQ(false, a.empty());
    a.set(0);
    EXPECT_EQ(true, a.any());
    EXPECT_EQ(false, a.none());
    EXPECT_EQ(false, a.empty());
    a.clear();
    EXPECT_EQ(false, a.any());
    EXPECT_EQ(true, a.none());
    EXPECT_EQ(true, a.empty());

}

TEST(BitSetTest, Clear){
    BitArray a = BitArray(70, 0);
    a.set();
    EXPECT_EQ("1111111111111111111111111111111111111111111111111111111111111111111111"
              , a.to_string());
    EXPECT_EQ(70, a.count());
    EXPECT_EQ(70, a.size());
    a.clear();
    EXPECT_EQ("", a.to_string());
    EXPECT_EQ(0, a.count());
    EXPECT_EQ(0, a.size());
}

TEST(BitSetTest, Equal){
    BitArray a = BitArray(100, 0);
    BitArray b = BitArray(a);
    for(int i = 0; i < a.size(); i++){
        a.set(i, true);
        b.set(i, true);
        EXPECT_EQ(true, a == b);
        EXPECT_EQ(false, a != b);
        EXPECT_EQ(a, b);
    }
    b.reset();
    EXPECT_EQ(false, a == b);
    EXPECT_EQ(true, a != b);
}

TEST(BitSetTest, LogicalAND){
    BitArray a = BitArray(2, 3);
    BitArray b = BitArray(2, 1);
    BitArray e = BitArray(3, 1);
    BitArray c = a & b;
    a &= b;
    EXPECT_EQ("01", c.to_string());
    EXPECT_EQ(1, c.count());
    EXPECT_EQ(true, a == c);
    
    a.clear(); b.clear(); c.clear();
    a.resize(100, false); b.resize(100, false); c.resize(100, false);
    for(int i = 0; i < 100; i += 2)
        a.set(i);
    for(int i = 0; i < 100; i += 3)
        b.set(i);
    c = a & b;
    for(int i = 0; i < 100; i += 6)
        EXPECT_EQ(true, c[i]);

    EXPECT_ANY_THROW(c = a & e);
}

TEST(BitSetTest, LogicalOR){
    BitArray a = BitArray(2, 3);
    BitArray b = BitArray(2, 1);
    BitArray e = BitArray(3, 1);
    BitArray c = a | b;
    a |= b;
    EXPECT_EQ("11", c.to_string());
    EXPECT_EQ(2, c.count());
    EXPECT_EQ(true, a == c);

    a.clear(); b.clear(); c.clear();
    a.resize(100, false); b.resize(100, false); c.resize(100, false);
    for(int i = 0; i < 100; i += 2)
        a.set(i);
    for(int i = 0; i < 100; i += 3)
        b.set(i);
    c = a | b;
    for(int i = 0; i < 100; i++)
        EXPECT_EQ( !((i % 2) && (i % 3)) , c[i]) << "Iteration: " << i << std::endl;

    EXPECT_ANY_THROW(c = a | e);
}

TEST(BitSetTest, LogicalXOR){
    BitArray a = BitArray(2, 3);
    BitArray b = BitArray(2, 1);
    BitArray e = BitArray(3, 1);
    BitArray c = a ^ b;
    a ^= b;
    EXPECT_EQ("10", c.to_string());
    EXPECT_EQ(1, c.count());
    EXPECT_EQ(true, a == c);

    a.clear(); b.clear(); c.clear();
    a.resize(100, false); b.resize(100, false); c.resize(100, false);
    for(int i = 0; i < 100; i += 2)
        a.set(i);
    for(int i = 0; i < 100; i += 3)
        b.set(i);
    c = a ^ b;
    for(int i = 2; i < 100; i += 6)
        EXPECT_EQ(true, c[i]); 
    for(int i = 3; i < 100; i += 6)
        EXPECT_EQ(true, c[i]); 
    for(int i = 4; i < 100; i += 6)
        EXPECT_EQ(true, c[i]); 
    for(int i = 5; i < 100; i += 6)
        EXPECT_EQ(false, c[i]);
    for(int i = 6; i < 100; i += 6)
        EXPECT_EQ(false, c[i]);
    for(int i = 7; i < 100; i += 6)
        EXPECT_EQ(false, c[i]);

    EXPECT_ANY_THROW(c = a ^ e);
}

TEST(BitSetTest, LogicalNOT){
    BitArray a = BitArray(70, 0);
    BitArray b = ~a;
    EXPECT_EQ("1111111111111111111111111111111111111111111111111111111111111111111111"
              , b.to_string());
    EXPECT_EQ(70, b.count());
    b.set(0, false);
    b = ~b;
    EXPECT_EQ("1000000000000000000000000000000000000000000000000000000000000000000000"
              , b.to_string());
    EXPECT_EQ(1, b.count());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}