#include <iostream>
#include <sstream>
#include <vector>

#define LONGBITS (sizeof(long) * 8)

class BitArray
{
    std::vector<long> bits;
    unsigned int numOfBits, countTrue;
    public:
        BitArray(); //TESTED
        ~BitArray(); //TESTED

        explicit BitArray(int num_bits, unsigned long value = 0); //TESTED
        BitArray(const BitArray& b); //TESTED

        void swap(BitArray& b); //TESTED
        
        BitArray& operator=(const BitArray& b); //TESTED

        void resize(int num_bits, bool value = false); //TESTED
        void clear(); //TESTED
        void push_back(bool bit); //TESTED

        BitArray& operator&=(const BitArray& b); //TESTED
        BitArray& operator|=(const BitArray& b); //TESTED
        BitArray& operator^=(const BitArray& b); //TESTED

        BitArray& operator<<=(int n); //TESTED
        BitArray& operator>>=(int n); //TESTED
        BitArray operator<<(int n) const; //TESTED
        BitArray operator>>(int n) const; //TESTED

        BitArray& set(int n, bool val = true); //TESTED
        BitArray& set(); //TESTED

        BitArray& reset(int n); //TESTED
        BitArray& reset(); //TESTED

        bool any() const; //TESTED
        bool none() const; //TESTED

        BitArray operator~() const; //TESTED

        int count() const; //TESTED

        bool operator[](int i) const; //TESTED

        int size() const; //TESTED
        bool empty() const; //TESTED

        std::string to_string() const; //TESTED
};

bool operator==(const BitArray & a, const BitArray & b); //TESTED
bool operator!=(const BitArray & a, const BitArray & b); //TESTED

BitArray operator&(const BitArray& b1, const BitArray& b2); //TESTED
BitArray operator|(const BitArray& b1, const BitArray& b2); //TESTED
BitArray operator^(const BitArray& b1, const BitArray& b2); //TESTED