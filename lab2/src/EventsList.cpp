#define DEBUG

#include "EventsList.h"
#include <chrono>
#include <thread>
#include <functional>
#include <X11/Xlib.h>

EventsList::EventsList(const Info &info) : 
    eventsCount(info.getCount()),
    runned(info.getCount()) {
    for(int i = 0; i < eventsCount; i++){
        std::string eventName = info.getEvent(i);
        std::string argument = info.getArg(i);
        auto task = EventFactory::Get()->CreateEvent(eventName, argument);
        list.insert(std::make_pair(info.getTime(i), std::make_pair(std::move(task), false)));
    }
}

void EventsList::run(const int startTime, const int endTime){
    std::thread t[eventsCount];
    int i = 0, step = 0;
    auto it = list.begin();
    if (startTime == 0 && endTime == 0) {

        for(; it != list.end(); ++it) {
            if(timeNow() > it->first) continue;
            std::this_thread::sleep_for(std::chrono::seconds(it->first - timeNow()));
            t[i++] = std::thread(&Event::run, std::move(it->second.first));
        } 

    } else {
        while (it->first < startTime && it != list.end()) {it++; std::cout << "xd ";}
        while( (it->first >= startTime ) && (it->first < endTime ) ){
            if (it == list.end()) break;
            if (it->second.second == true) continue;             
            XInitThreads();
            t[i++] = std::thread(&Event::run, std::move(it->second.first));
            it->second.second = true;
            it++;                
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    for(i = 0; i < eventsCount; i++)
        if(t[i].joinable()) t[i].join();

    initRunned();
}

long EventsList::timeNow(){
    auto now = std::chrono::system_clock::now();
    auto now_s = std::chrono::time_point_cast<std::chrono::seconds>(now);
    auto value = now_s.time_since_epoch();
    return (value.count() + 25200) % 86400;
}

void EventsList::initRunned(){
    int i = 0;
    for(auto& it : list) runned.set(i++, it.second.second);
}

std::string EventsList::getRunned(){
    return runned.to_string();
}