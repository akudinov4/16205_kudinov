#include "MusicPlayer.h"

MusicPlayer::MusicPlayer(std::string argument): arg(argument){}

void MusicPlayer::run(){
    sf::Music music;
    if (!music.openFromFile(arg)){
        throw std::exception();
    }
    music.setVolume(100);
    music.play();
    sf::sleep(music.getDuration());
}