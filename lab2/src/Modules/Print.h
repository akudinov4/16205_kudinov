#ifndef PRINT

#define PRINT

#include "../Event.h"

class PrintMessage : public Event{
    std::string arg;
public:
    PrintMessage(std::string argument);
    ~PrintMessage() = default;
    void run();

    static std::unique_ptr<Event> Create(std::string arg) { return std::make_unique<PrintMessage>(arg); }
};

#endif