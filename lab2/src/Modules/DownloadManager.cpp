#include "DownloadManager.h"
#include "Print.h"

DownloadManager::DownloadManager(std::string argument){
    if(argument.find(":") == -1){
        host = argument.substr(0, argument.find_first_of("/"));
        uri = argument.substr(argument.find_first_of("/") + 1, argument.find_first_of(" ") - argument.find_first_of("/") - 1);
        filename = argument.substr(argument.find_first_of(" ") + 1, argument.length());
    }
}

void DownloadManager::run(){
    sf::Http http;

    http.setHost(host);
    
    sf::Http::Request request;
    request.setUri(uri);
    sf::Http::Response response = http.sendRequest(request);
    if (response.getStatus() == sf::Http::Response::Ok) {
        std::ofstream file(filename, std::ios::out | std::ios::binary);
        file << response.getBody();
        file.close();
        std::string text = "Downloaded " + host + "/" + uri + " to " + filename;
        PrintMessage print(text);
        print.run();
    } else {
        std::cout << "Error: " << response.getStatus() << std::endl;
    }
}