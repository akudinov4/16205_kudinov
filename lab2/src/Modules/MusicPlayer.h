#ifndef MUSICPLAYER

#define MUSICPLAYER

#include "../Event.h"

class MusicPlayer : public Event {
    std::string arg;
public:
    MusicPlayer(std::string argument);
    ~MusicPlayer() = default;
    void run();

    static std::unique_ptr<Event> Create(std::string arg) { return std::make_unique<MusicPlayer>(arg); }
};
#endif