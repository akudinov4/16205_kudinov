#include "Print.h"

PrintMessage::PrintMessage(std::string argument): arg(argument){
}

void PrintMessage::run(){
    if (arg == "") return;
    sf::Vector2i grabbedOffset;
    bool grabbedWindow = false;
    int width = (arg.length() < 15)?320:(320 + arg.length() * 10);
    
    sf::RenderWindow window(sf::VideoMode(width, 140), "Task Manager notify", sf::Style::None);

    sf::Font font;
    font.loadFromFile("Resources/ArialNarrow.ttf");

    sf::Text text(arg, font);
    text.setCharacterSize((arg.length() < 15)?40:35);
    text.setColor(sf::Color::Black);

    sf::FloatRect textRect = text.getLocalBounds();
    text.setOrigin(textRect.left + textRect.width/2.0f,
                   textRect.top  + textRect.height * 2.0f);
    text.setPosition(sf::Vector2f(width/2.0f,180/2.0f));

    sf::Text ok("OK", font);
    ok.setCharacterSize(30);
    ok.setColor(sf::Color::Black);

    sf::FloatRect okRect = ok.getLocalBounds();
    ok.setOrigin(okRect.left + okRect.width/2.0f,
                okRect.top);
    ok.setPosition(sf::Vector2f(width/2.0f,180/2.0f));

    window.clear(sf::Color(215, 222, 225, 125));
    window.draw(text);
    window.draw(ok);
    window.display();
    
    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::MouseMoved:
                {
                    sf::Vector2i mousePos = sf::Mouse::getPosition(window);
                    sf::Vector2f mousePosF(static_cast<float>(mousePos.x), static_cast<float>(mousePos.y));
                    if (ok.getGlobalBounds().contains(mousePosF))
                        ok.setColor(sf::Color(sf::Color::White));
                    else
                        ok.setColor(sf::Color(sf::Color::Black));
                    
                    if (grabbedWindow)
                        window.setPosition(sf::Mouse::getPosition(window) + grabbedOffset);
                }
                break;
            case sf::Event::MouseButtonPressed:
                {
                    sf::Vector2i mousePos = sf::Mouse::getPosition(window);
                    sf::Vector2f mousePosF( static_cast<float>( mousePos.x ), static_cast<float>( mousePos.y ) );
                    if ( ok.getGlobalBounds().contains( mousePosF ) )
                    {
                        window.close();
                    } else if (event.mouseButton.button == sf::Mouse::Left) {
                        grabbedOffset = window.getPosition() - sf::Mouse::getPosition(window);
                        grabbedWindow = true;
                    }
                }
                break;
            case sf::Event::MouseButtonReleased:
                    grabbedWindow = false;
                break;
            }
            window.clear(sf::Color(215, 222, 225, 128));  
            window.draw(text);
            window.draw(ok);
            window.display();
        }
    }
}