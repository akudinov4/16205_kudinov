#ifndef DOWNLOADMANAGER

#define DOWNLOADMANAGER

#include "../Event.h"

class DownloadManager : public Event{
    std::string host;
    std::string uri;
    std::string filename;
public:
    DownloadManager(std::string argument);
    ~DownloadManager() = default;
    void run();

    static std::unique_ptr<Event> Create(std::string arg) { return std::make_unique<DownloadManager>(arg); }
};

#endif