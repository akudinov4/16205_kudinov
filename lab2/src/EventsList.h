#ifndef EVENTSLIST 

#define EVENTSLIST

#include "Info.h"
#include "EventFactory.h"
#include "../../lab1/bitset.h"

class EventsList{
    std::multimap <int, std::pair<std::unique_ptr<Event>, bool>> list;
    BitArray runned;
    int eventsCount;

public:
    EventsList() = default;
    ~EventsList() = default;

    EventsList(const Info &information);
    
    void run(const int startTime = 0, const int endTime = 0);

    std::string getRunned();
private:
    void initRunned();
    long timeNow();
};

#endif