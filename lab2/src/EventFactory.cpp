#include "EventFactory.h"

EventFactory::EventFactory(){
    Register("print", &PrintMessage::Create);
    Register("play", &MusicPlayer::Create);
    Register("download", &DownloadManager::Create);
}

void EventFactory::Register(const std::string &eventName, std::function<std::unique_ptr<Event>(std::string)> CreateEvent)
{
    m_EventFactory.insert(std::make_pair(eventName, std::move(CreateEvent)));
}

std::unique_ptr<Event> EventFactory::CreateEvent(const std::string name, std::string arguments){
    FactoryMap::iterator it = m_EventFactory.find(name);
    if( it != m_EventFactory.end() ) return it->second(arguments);
    return NULL;
}