#include "EventsList.h"
#include <gtest/gtest.h>

TEST(Info, addEvent){
    Info test("");
    test.addEvent(200, "print", "test");
    EXPECT_EQ(test.getCount(), 1);
    EXPECT_EQ(test.getTime(0), 200);
    EXPECT_EQ(test.getEvent(0), "print");
    EXPECT_EQ(test.getArg(0), "test");
    EXPECT_ANY_THROW(test.addEvent(90000, "print", "test"));
    EXPECT_ANY_THROW(test.addEvent(-1, "print", "test"));
    EXPECT_ANY_THROW(test.addEvent(90, "test", "test"));
}

TEST(Info, init){
    EXPECT_ANY_THROW(Info exceptionTest("hah"));
}

TEST(Info, readFile){
    Info test("test.txt");
    test.readFile();
    EXPECT_EQ(test.getCount(), 2);
    EXPECT_EQ(test.getTime(0), 600);
    EXPECT_EQ(test.getEvent(0), "print");
    EXPECT_EQ(test.getArg(0), "Test");
    EXPECT_EQ(test.getTime(1), 1800);
    EXPECT_EQ(test.getEvent(1), "download");
    EXPECT_EQ(test.getArg(1), "www.phys.nsu.ru/fit/Zadanie_1_2017.pdf Zadanie_1_2017.pdf");
}

TEST(EventList, test){
    Info test("test.txt");
    test.readFile();
    EventsList list(test);
    list.run(0, 20000);
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}