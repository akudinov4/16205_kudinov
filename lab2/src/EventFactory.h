#ifndef EVENTFACTORY

#define EVENTFACTORY

#include "Event.h"
#include "Modules/MusicPlayer.h"
#include "Modules/Print.h"
#include "Modules/DownloadManager.h"
#include <functional>

class EventFactory{
private:    
    EventFactory();
    typedef std::map<std::string, std::function<std::unique_ptr<Event>(std::string)>> FactoryMap;
    FactoryMap m_EventFactory;

public:
    ~EventFactory() = default;

    static EventFactory* Get(){
        static EventFactory instance;
        return &instance;
    }

    void Register(const std::string &EventName, std::function<std::unique_ptr<Event>(std::string)> CreateEvent);
    std::unique_ptr<Event> CreateEvent(const std::string name, std::string arguments);

};

#endif