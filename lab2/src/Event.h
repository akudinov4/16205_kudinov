#ifndef EVENT

#define EVENT

#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <map>
#include <memory>
#include <fstream>

class Event{
public:
    virtual void run() {};
};

#endif