#include "Info.h"

Info::Info(std::string filename) : 
    filename(filename), 
    eventsCount(0),
    time(0),
    event(0),
    args(0){
    if (filename == "") return;
    file.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    try{
        file.open(filename);
    } catch(std::ifstream::failure& e) {
        throw std::runtime_error("Failed to open file");
        return;
    }

}

void Info::checkFile(){
    std::string tmpStr;
    while(file.good()){
        file >> tmpStr;
        if (tmpStr == "") throw std::exception();
        if (tmpStr.find(":") != 2 && tmpStr.find(":", 4) != 4) throw std::exception();
        file >> tmpStr;
        if (tmpStr != "print" && tmpStr != "play" && tmpStr != "download") throw std::exception();
        std::getline(file, tmpStr);
        if (tmpStr.find(":") != -1) throw std::exception();
    }
    file.clear();
    file.seekg(0, file.beg);
}

void Info::readFile(){
    std::string tmpStr;
    int seconds;
    try {
        checkFile();
    } catch (std::exception& e) {
        std::cout << "Wrong file format" << std::endl;
        return;
    }
    while(file.good()){
        file >> tmpStr;
        try{
            seconds = convertTime(tmpStr);
        } catch(std::exception& e){
            file >> tmpStr;
            std::cout << "Wrong time for event \"" << tmpStr << "\" with argument ";
            file.get();
            std::getline(file, tmpStr);
            std::cout << "\"" << tmpStr << "\"" << std::endl;
            continue;
        }
        time.push_back(convertTime(tmpStr));
        file >> tmpStr;
        event.push_back(tmpStr);
        file.get();
        std::getline(file, tmpStr);
        args.push_back(tmpStr);
        eventsCount++;
    }
    file.close();
}

void Info::addEvent(int evTime, std::string eventType, std::string arg){
    if(evTime > 86400 || evTime < 0) throw std::exception();
    if (eventType != "print" && eventType != "play" && eventType != "download") throw std::exception();
    time.push_back(evTime);
    event.push_back(eventType);
    args.push_back(arg);
    eventsCount++;
}

int Info::convertTime(std::string strTime){
    int hours = stoi(strTime.substr(0, 2));
    int minutes = stoi(strTime.substr(strTime.find_first_of(":") + 1, 2));
    int seconds = stoi(strTime.substr(strTime.find_last_of(":") + 1, 2));
    if (hours >= 24 || minutes >= 60 || seconds >= 60) throw std::exception();
    return 3600 * hours + 60 * minutes + seconds;
}

void Info::testPrint(){
    for(int i = 0; i < eventsCount; i++)
    std::cout << time[i] << "|" << event[i] << "|" << args[i] << std::endl;
}

int Info::getCount() const{
    return eventsCount;
}

int Info::getTime(int num) const{
    return time.at(num);
}

std::string Info::getEvent(int num) const{
    return event.at(num);
}

std::string Info::getArg(int num) const{
    return args.at(num);
}