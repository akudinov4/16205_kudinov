#ifndef INFO

#define INFO

#include <iostream>
#include <map>
#include <fstream>
#include <vector>
#include <string>
#include <exception>

class Info{
    std::string filename;
    std::ifstream file;
    std::vector<int> time;
    std::vector<std::string> event;
    std::vector<std::string> args;
    int eventsCount;
    
public:
    Info() = default;
    ~Info() = default;

    Info(std::string filename = "");

    void checkFile();
    void readFile();
    void testPrint();

    int getCount() const;
    
    int getTime(int num) const;
    std::string getEvent(int num) const;
    std::string getArg(int num) const;

    void addEvent(int time, std::string event, std::string arg);

private:
    int convertTime(std::string strTime);
};

#endif