#ifndef BATTLEPROP

#define BATTLEPROP

#include "../../Object.h"

class Object;

struct BattleStats {
    int maxHealth;
    int curHealth;
    int armor;
    int baseDamage;
    std::shared_ptr<Object> target;
};

struct BattleRender{
    sf::RectangleShape object;
    sf::Text HP;
};

#endif