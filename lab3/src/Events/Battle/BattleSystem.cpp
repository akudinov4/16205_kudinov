#include "BattleSystem.h"

void Battle::prepareEvent(std::map<int, std::shared_ptr<Object>> &units)
{
    mFont.loadFromFile("Resources/ArialNarrow.ttf");
    prepareRender();
    std::cout << std::endl
              << "Initialization of Battle" << std::endl;
    for (auto it : units)
    {
        if (it.second->getProperty<Player>() != nullptr)
        {
            std::string strHP = std::to_string(it.second->getProperty<BattleStats>()->curHealth) + "/" + std::to_string(it.second->getProperty<BattleStats>()->maxHealth);
            sf::Text tmpHP(strHP, mFont, 20);
            it.second->addProperty(std::shared_ptr<BattleRender>(new BattleRender));
            it.second->getProperty<BattleRender>()->object = it.second->getProperty<Render>()->object;
            tmpHP.setPosition(70, 250);
            tmpHP.setColor(sf::Color::White);
            std::cout << "Add Player with name " << it.second->getName() << std::endl;
            it.second->getProperty<BattleRender>()->object.setPosition(100, 200);
            it.second->getProperty<BattleRender>()->HP = tmpHP;
            mPlayers.push_back(it.second);
            continue;
        }
        if (it.second->getProperty<Enemy>() != nullptr)
        {
            std::string strHP = std::to_string(it.second->getProperty<BattleStats>()->curHealth) + "/" + std::to_string(it.second->getProperty<BattleStats>()->maxHealth);
            sf::Text tmpHP(strHP, mFont, 20);
            tmpHP.setPosition(480, 250);
            it.second->addProperty(std::shared_ptr<BattleRender>(new BattleRender));
            it.second->getProperty<BattleRender>()->object = it.second->getProperty<Render>()->object;
            std::cout << "Add Enemy with name " << it.second->getName() << std::endl;
            it.second->getProperty<BattleRender>()->object.setPosition(500, 200);
            it.second->getProperty<BattleRender>()->HP = tmpHP;
            mEnemies.push_back(it.second);
        }
    }
    turn = mPlayers[0];
}

void Battle::startEvent()
{
}

bool Battle::allPlayersDead()
{
    bool res = true;
    for (auto it : mPlayers)
    {
        res &= (it->getProperty<BattleStats>()->curHealth > 0) ? false : true;
    }
    return res;
}

bool Battle::allEnemiesDead()
{
    bool res = true;
    for (auto it : mEnemies)
    {
        res &= (it->getProperty<BattleStats>()->curHealth > 0) ? false : true;
    }
    return res;
}

std::unique_ptr<Event> Battle::isEventEnd()
{
    std::unique_ptr<Event> newEvent = nullptr;

    if (allPlayersDead())
    {
        mPlayers[0]->getProperty<FieldStats>()->alive = false;
        newEvent = EventFactory::Get()->CreateEvent("Field");
        newEvent->prepareEvent(ObjectManager::Get()->getAllObjects());
    }
    if (allEnemiesDead())
    {
        mEnemies[0]->getProperty<FieldStats>()->alive = false;
        newEvent = EventFactory::Get()->CreateEvent("Field");
        newEvent->prepareEvent(ObjectManager::Get()->getAllObjects());
        mPlayers[0]->getProperty<BaseStats>()->xp += Rule::xpFromUnit(mEnemies[0]);
        Rule::levelUpCheck(mPlayers[0]);
        std::cout << "Now your level: " << mPlayers[0]->getProperty<BaseStats>()->level << std::endl;
    }
    return newEvent;
}

void Battle::PrintAvailableTargets()
{
    int count = 0;
    std::cout << "Available targets: " << std::endl;
    for (auto it : mEnemies)
    {
        if (it->getProperty<BattleStats>()->curHealth > 0)
            std::cout << count << " : "
                      << it->getName() << " have "
                      << it->getProperty<BattleStats>()->curHealth
                      << " HP" << std::endl;
        count++;
    }
    std::cout << std::endl;
}

void Battle::prepareRender()
{
    sf::RectangleShape tmp;
    sf::Text tmpT("", mFont);
    tmp.setSize(sf::Vector2f(640, 200));
    tmp.setPosition(0, 280);
    tmp.setFillColor(sf::Color::Blue);
    mMenu.insert(std::make_pair(0, std::make_pair(tmp, tmpT)));
    tmp.setSize(sf::Vector2f(160, 40));
    tmp.setPosition(40, 320);
    tmp.setFillColor(sf::Color::Black);
    tmpT.setString("Select Target");
    tmpT.setPosition(sf::Vector2f(44, 320));
    tmpT.setColor(sf::Color::Cyan);
    mMenu.insert(std::make_pair(10, std::make_pair(tmp, tmpT)));
    tmp.setSize(sf::Vector2f(160, 40));
    tmp.setPosition(40, 380);
    tmp.setFillColor(sf::Color::Black);
    tmpT.setString("Attack");
    tmpT.setPosition(sf::Vector2f(85, 380));
    tmpT.setColor(sf::Color::Cyan);
    mMenu.insert(std::make_pair(11, std::make_pair(tmp, tmpT)));
}

void Battle::render(sf::RenderWindow &mWindow)
{
    mWindow.clear();
    for (auto& it : mPlayers)
    {
        mWindow.draw(it->getProperty<BattleRender>()->object);
        mWindow.draw(it->getProperty<BattleRender>()->HP);
    }
    for (auto& it : mEnemies)
    {
        mWindow.draw(it->getProperty<BattleRender>()->object);
        mWindow.draw(it->getProperty<BattleRender>()->HP);
    }
    mWindow.draw(mMenu[0].first);
    for (auto& it : mMenu)
    {
        if (it.first >= getMenuNum() && it.first <= getMenuNum() + 10)
        {
            mWindow.draw(it.second.first);
            mWindow.draw(it.second.second);
        }
    }
    mWindow.display();
}

void Battle::processEvent(sf::RenderWindow &mWindow)
{
    sf::Event event;
    while (mWindow.pollEvent(event))
    {
        int menuNum = getMenuNum();
        if (turn == mPlayers[0])
        {
            sf::Vector2i mousePos = sf::Mouse::getPosition(mWindow);
            sf::Vector2f mousePosF(static_cast<float>(mousePos.x), static_cast<float>(mousePos.y));
            if (event.type == sf::Event::MouseMoved)
            {
                for (auto &it : mMenu)
                {
                    if (it.first >= menuNum && it.first <= menuNum + 10)
                    {
                        if (it.second.first.getGlobalBounds().contains(mousePosF))
                        {
                            it.second.first.setFillColor(sf::Color(sf::Color::White));
                        }
                        else
                        {
                            it.second.first.setFillColor(sf::Color(sf::Color::Black));
                        }
                    }
                }
                if (MENU_STATE == SELECT_TARGET)
                {
                    for (int i = 0; i < mEnemies.size(); ++i)
                    {
                        auto it = mEnemies[i];
                        if (it->getProperty<BattleRender>()->object.getGlobalBounds().contains(mousePosF))
                        {
                            it->getProperty<BattleRender>()->object.setOutlineColor(sf::Color(sf::Color::White));
                            it->getProperty<BattleRender>()->object.setOutlineThickness(5.0f);
                        }
                        else
                        {
                            it->getProperty<BattleRender>()->object.setOutlineThickness(0.0f);
                        }
                    }
                }
            }
            if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
            {
                for (auto it : mMenu)
                {
                    if (it.first >= menuNum && it.first <= menuNum + 10)
                    {
                        if (it.second.first.getGlobalBounds().contains(mousePosF))
                        {
                            if (it.first == 10)
                                MENU_STATE = SELECT_TARGET;
                            if (it.first == 11 && target != NULL)
                            {
                                Rule::attack(turn, target);
                                nextTurn();
                            }
                        }
                    }
                }
                if (MENU_STATE == SELECT_TARGET)
                    for (int i = 0; i < mEnemies.size(); ++i)
                    {
                        auto it = mEnemies[i];
                        if (it->getProperty<BattleRender>()->object.getGlobalBounds().contains(mousePosF))
                        {
                            setTarget(i);
                            it->getProperty<BattleRender>()->object.setOutlineThickness(0.0f);
                            MENU_STATE = MENU;
                            std::cout << "Selected target: " << it->getName() << std::endl;
                        }
                    }
            }
        }
        else
        {
            Rule::attack(turn, mPlayers[0]);
            nextTurn();
        }
    }
}

int Battle::getMenuNum()
{
    return 10 + MENU_STATE * 10;
}

void Battle::setTarget(int ID)
{
    target = mEnemies[ID];
}

void Battle::update(sf::Time deltaTime){

}

void Battle::nextTurn()
{
    if (turn->getProperty<Player>() != nullptr && turn != mPlayers[0])
    {
        for (int i = 0; i < mPlayers.size(); ++i)
            if (turn == mPlayers[i])
                turn = mPlayers[i + 1];
        return;
    }
    if (turn->getProperty<Player>() != nullptr)
    {
        turn = mEnemies[0];
        return;
    }
    if (turn->getProperty<Enemy>() != nullptr && turn != mEnemies[0])
    {
        for (int i = 0; i < mEnemies.size(); ++i)
            if (turn == mEnemies[i])
                turn = mEnemies[i + 1];
        return;
    }
    if (turn->getProperty<Enemy>() != nullptr)
        turn = mPlayers[0];
}