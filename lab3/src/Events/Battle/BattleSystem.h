#ifndef BATTLE

#define BATTLE

#include "../../Event.h"
#include "../../EventFactory.h"
#include "../../Rules.h"
#include "../../ObjectManager.h"
#include "BattleProp.h"


class Battle : public Event{
    std::vector<std::shared_ptr<Object>> mPlayers;
    std::vector<std::shared_ptr<Object>> mEnemies;

    sf::Font mFont;

    std::map<int ,std::pair<sf::RectangleShape, sf::Text>> mMenu;

    std::shared_ptr<Object> target = NULL;

    int MENU_STATE = 0;
    const int MENU = 0;
    const int SELECT_TARGET = 1;

    std::shared_ptr<Object> turn;

public:

    Battle() = default;
    ~Battle() = default;
    void prepareEvent(std::map<int, std::shared_ptr<Object>> & objects);
    void startEvent();
    void render(sf::RenderWindow&);
    void processEvent(sf::RenderWindow&);
    void update(sf::Time deltaTime);
    static std::unique_ptr<Event> Create() { return std::make_unique<Battle>(); }

    std::unique_ptr<Event> isEventEnd();

private:

    void prepareRender();

    bool allPlayersDead();
    bool allEnemiesDead();
    void setTarget(int ID);

    void PrintAvailableTargets();

    int getMenuNum();
    void nextTurn();

};

int createBattle = EventFactory::Get()->Register("Battle", &Battle::Create);

#endif