#include "Field.h"

void Field::prepareEvent(std::map<int, std::shared_ptr<Object>> & objects){
    std::vector<int> IDs = {0, 1, 2, 3}; // Потом список ID будет выдавать парсер
    for(auto it: objects){
        mFieldObjects.push_back(it.second);
    }
}

void Field::render(sf::RenderWindow& window){
    window.clear();
    for(auto& it : mFieldObjects)
        if(it->getProperty<FieldStats>()->alive) 
            window.draw(it->getProperty<Render>()->object);
    if(mIsSubEvent) mSubEvent->render(window);
    window.display();
}

void Field::startEvent(){
    
}

void Field::processEvent(sf::RenderWindow& window){
    if(mIsSubEvent){
        mSubEvent->processEvent(window);
        return;
    }
    sf::Event event;
    while (window.pollEvent(event)){
        if (event.type == sf::Event::Closed || event.key.code == sf::Keyboard::Escape)
            window.close();
        if (event.type == sf::Event::KeyPressed)
            handlerPlayerInput(event.key.code, true);
        if (event.type == sf::Event::KeyReleased)
            handlerPlayerInput(event.key.code, false);
    }
}

void Field::update(sf::Time deltaTime){
    if(mIsSubEvent){
        mSubEvent->update(deltaTime);
        if(mSubEvent->isEventEnd() != nullptr) {
            mIsSubEvent = false;
        }
        return;
    }
    checkDialog();
    Rule::move(ObjectManager::Get()->getObject(PLAYER), deltaTime);
    
}

void Field::handlerPlayerInput(sf::Keyboard::Key key, bool isPressed){
    auto stats = mFieldObjects[0]->getProperty<FieldStats>();
    if(key == sf::Keyboard::W)
        stats->mIsMovingUp = isPressed;
    else if(key == sf::Keyboard::S)
        stats->mIsMovingDown = isPressed;
    else if(key == sf::Keyboard::A)
        stats->mIsMovingLeft = isPressed;
    else if(key == sf::Keyboard::D)
        stats->mIsMovingRight = isPressed;
    else if(key == sf::Keyboard::E)
        stats->mAction = isPressed;
    else if(key == sf::Keyboard::Tab && isPressed)
        Rule::printStats();
}

std::unique_ptr<Event> Field::checkBattle()
{
    int col = calculateCollisions<Enemy>();
    std::unique_ptr<Event> mEvent = nullptr;
    if (col != 0)
    {
        for(auto it: mFieldObjects) it->getProperty<FieldStats>()->stop();
        mEvent = EventFactory::Get()->CreateEvent("Battle");
        std::map<int, std::shared_ptr<Object>> forBattle = {{0, mFieldObjects[PLAYER]}, {1, mFieldObjects[col]}};
        mEvent->prepareEvent(forBattle);
    }
    return mEvent;
}

void Field::checkItem()
{
    int col = calculateCollisions<Item>();
    if (col != 0 && mFieldObjects[PLAYER]->getProperty<FieldStats>()->mAction)
    {
        Rule::pickupItem(mFieldObjects[PLAYER], mFieldObjects[col]);
    }
}

void Field::checkDialog(){
    int col = calculateCollisions<NPC>();
    if (col != 0 && mFieldObjects[PLAYER]->getProperty<FieldStats>()->mAction){
        mFieldObjects[PLAYER]->getProperty<FieldStats>()->mAction = false;
        mSubEvent = EventFactory::Get()->CreateEvent("Dialog");
        std::map<int, std::shared_ptr<Object>> forDialog = {{0, mFieldObjects[PLAYER]}, {1, mFieldObjects[col]}};
        mSubEvent->prepareEvent(forDialog);
        mIsSubEvent = true;
    }
}

std::unique_ptr<Event> Field::isEventEnd(){
    auto res = checkBattle();
    return res;
}