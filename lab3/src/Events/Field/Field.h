#ifndef FIELD

#define FIELD

#include "../../Event.h"
#include "../../Game.h"
#include "../../EventFactory.h"
#include "../../Rules.h"

class Field : public Event{
    int mID;
    std::vector<int> mIDs;
    std::vector<std::pair<int, sf::RectangleShape>> mGoPoints;
    std::vector<sf::Vector2f> mSpawnPoints;
    std::vector<std::shared_ptr<Object>> mFieldObjects;

public:
    void prepareEvent(std::map<int, std::shared_ptr<Object>> &);
    void startEvent();
    void render(sf::RenderWindow&);
    void processEvent(sf::RenderWindow&);
    void update(sf::Time deltaTime);
    std::unique_ptr<Event> isEventEnd();

    static std::unique_ptr<Event> Create() { return std::make_unique<Field>(); }

private:
    void handlerPlayerInput(sf::Keyboard::Key key, bool isPressed);
    std::unique_ptr<Event> checkBattle();
    void checkItem();
    void checkDialog();

    template<class T> int calculateCollisions(){
        auto player = mFieldObjects[PLAYER]->getProperty<Render>()->object.getGlobalBounds();
        for(int i = 1; i < mFieldObjects.size(); i++){
            if(mFieldObjects[i]->getProperty<T>() == nullptr) continue;
            auto enemy = mFieldObjects[i]->getProperty<Render>()->object.getGlobalBounds();
            auto enemyAlive = mFieldObjects[i]->getProperty<FieldStats>()->alive;
            auto playerAlive = mFieldObjects[PLAYER]->getProperty<FieldStats>()->alive;
            if (player.intersects(enemy) && enemyAlive && playerAlive)
                return i;
            }
        return 0;
    };
};

int createField = EventFactory::Get()->Register("Field", &Field::Create);

#endif