

struct FieldStats{
    int moveSpeed = 400;
    bool mIsMovingUp = 0;
    bool mIsMovingDown = 0;
    bool mIsMovingLeft = 0;
    bool mIsMovingRight = 0;
    bool mAction = 0;
    bool alive = 1;
    void stop() { mIsMovingDown = 0; mIsMovingUp = 0; mIsMovingLeft = 0; mIsMovingRight = 0;};
};

struct BaseStats{
    int xp = 0;
    int level = 1;
    int strength = 10;
    int speed = 10;
    int intelligence = 10;
};