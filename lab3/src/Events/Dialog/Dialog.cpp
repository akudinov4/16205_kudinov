#include "Dialog.h"

void Dialog::prepareEvent(std::map<int, std::shared_ptr<Object>> & objects){
    for(auto it: objects){
        it.second->getProperty<Speech>()->setSpeech(0);
        mObjects.push_back(it.second);
    }
    prepareRender();
}

void Dialog::prepareRender(){
    mFont.loadFromFile("Resources/ArialNarrow.ttf");
    sf::RectangleShape tmp;
    sf::Text tmpT("", mFont);
    sf::Color tmpColor = sf::Color(0, 0, 255, 128);
    tmp.setSize(sf::Vector2f(640, 200));
    tmp.setPosition(0, 280);
    tmp.setFillColor(tmpColor);
    mMenu.insert(std::make_pair(0, std::make_pair(tmp, tmpT)));
    for(auto& it : mObjects){
        it->getProperty<DialogRender>()->object = it->getProperty<Render>()->object;
        if(it->getProperty<Player>() != nullptr){
            it->getProperty<DialogRender>()->object.setScale(3, 3);
            it->getProperty<DialogRender>()->object.setPosition(50, 100);
        } else {
            it->getProperty<DialogRender>()->object.setScale(3, 3);
            it->getProperty<DialogRender>()->object.setPosition(550, 100);
        }
    }
}

void Dialog::render(sf::RenderWindow& window){
    for(auto& it: mObjects){
        window.draw(it->getProperty<DialogRender>()->object);
    }
    for(auto& it: mMenu){
        window.draw(it.second.first);
    }
    window.draw(mCurText);
}

void Dialog::processEvent(sf::RenderWindow& window){
    sf::Event event;
    while(window.pollEvent(event)){
        if (event.type == sf::Event::Closed || event.key.code == sf::Keyboard::Escape)
            window.close();
        if (event.key.code == sf::Keyboard::Q)
            mIsEnd = true;
        if (event.key.code == sf::Keyboard::E && event.type == sf::Event::KeyPressed)
            mIsNext = true;
    }
}

void Dialog::update(sf::Time deltaTime){
    mCurText = mObjects[1]->getProperty<Speech>()->getText();
    if(mCurText.getString() == "") mIsEnd = true;
    mCurText.setPosition(100, 300);
    mCurText.setScale(2, 3);
    if(mIsNext){
        mObjects[1]->getProperty<Speech>()->nextSpeech(mObjects[1]->getProperty<Speech>()->test);
        mIsNext = false;
    } 
}

std::unique_ptr<Event> Dialog::isEventEnd(){
    if(mIsEnd) return std::unique_ptr<Event>(new Dialog);
    return nullptr;
}

void Dialog::startEvent(){

}

