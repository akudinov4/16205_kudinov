#ifndef DIALOGPROP

#define DIALOGPROP

#include "../../Object.h"
#include "../../ObjectManager.h"

class Object;
struct Enemy;
struct BattleStats;

struct DialogRender{
    sf::RectangleShape object;
};

class Speech{

    sf::Font mFont;
    int mCurText = 0;
    std::map<int, sf::Text> mTexts;
    std::map<int, std::vector<std::pair<int, sf::Text>>> mAnswers;
    std::map<int, std::function<void()>> mFunctions;
public:
    Speech();
    ~Speech() = default;
    int test = 0;
    void nextSpeech(int ansID);
    sf::Text getText() const;
    const std::vector<std::pair<int, sf::Text>>& getAnswers();
    void setSpeech(int speechID);
};

#endif