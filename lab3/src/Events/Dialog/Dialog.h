#include "../../Event.h"
#include "../../EventFactory.h"
#include "DialogProp.h"

class Dialog : public Event{

    std::vector<std::shared_ptr<Object>> mObjects;
    sf::Text mCurText;
    bool mIsEnd = false;
    bool mIsNext = false;

    sf::Font mFont;

    std::map<int ,std::pair<sf::RectangleShape, sf::Text>> mMenu;

public:
    Dialog() = default;
    ~Dialog() = default;
    void prepareEvent(std::map<int, std::shared_ptr<Object>> & Objects);
    void startEvent();
    void render(sf::RenderWindow&);
    void processEvent(sf::RenderWindow&);
    void update(sf::Time deltaTime);
    static std::unique_ptr<Event> Create() { return std::make_unique<Dialog>(); }

    std::unique_ptr<Event> isEventEnd();

private:

    void prepareRender();
};

int createDialog = EventFactory::Get()->Register("Dialog", &Dialog::Create);