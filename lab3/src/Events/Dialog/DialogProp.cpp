#include "DialogProp.h"

void foo() { 
        std::shared_ptr<Object> hero = ObjectManager::Get()->getObject(0);
        hero->getProperty<BattleStats>()->baseDamage += 250;
        ObjectManager::Get()->getObject(4)->getProperty<Speech>()->test++;
    }
void foo2(){
        std::shared_ptr<Object> NPC = ObjectManager::Get()->getObject(4);
        NPC->addProperty(std::shared_ptr<Enemy>(new Enemy));
    }

Speech::Speech(){
    mFont.loadFromFile("Resources/ArialNarrow.ttf");
    sf::Text tmpText("Hello", mFont, 20);
    mTexts.insert(std::make_pair(0, tmpText));
    tmpText.setString("Are you an enemy or a friend?");
    mTexts.insert(std::make_pair(1, tmpText));
    std::vector<std::pair<int, sf::Text>> tmpVect;
    tmpText.setString("I'm your friend");
    tmpVect.push_back(std::make_pair(2, tmpText));
    tmpText.setString("Ha-ha-ha");
    tmpVect.push_back(std::make_pair(4, tmpText));
    mAnswers.insert(std::make_pair(1, std::move(tmpVect)));
    tmpText.setString("Oh, It's good. Take this");
    mTexts.insert(std::make_pair(2, tmpText));
    tmpText.setString("I kill you!!!");
    mTexts.insert(std::make_pair(4, tmpText));
    mFunctions.insert(std::make_pair(2, foo));
    mFunctions.insert(std::make_pair(5, foo2));
}

sf::Text Speech::getText() const{
    auto it = mTexts.find(mCurText);
    if(it == mTexts.end()) return sf::Text("", mFont, 20);
    return it->second;
}

void Speech::nextSpeech(int ansID){
    auto itAns = mAnswers.find(mCurText);
    if(itAns == mAnswers.end()) mCurText++;
    else {
        mCurText = itAns->second.at(ansID).first;
    }
    std::cout << mCurText << std::endl;
    auto itF = mFunctions.find(mCurText);
    if(itF != mFunctions.end()) itF->second();
    sf::sleep(sf::seconds(0.2f));
}

void Speech::setSpeech(int set){
    mCurText = set;
}

const std::vector<std::pair<int, sf::Text>>& Speech::getAnswers(){
    auto itAns = mAnswers.find(mCurText);
    return itAns->second;
}