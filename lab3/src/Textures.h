#ifndef TEXTURES

#define TEXTURES

#include "Object.h"

class TextureHolder{
    std::map<int, std::unique_ptr<sf::Texture>> mTextures;

public:
    TextureHolder() = default;
    ~TextureHolder() = default;

    void loadTexture(int ID, const std::string &filename);
    void clear();
};

#endif