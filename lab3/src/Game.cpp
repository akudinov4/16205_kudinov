#include "Game.h"
#include "EventFactory.h"

Game::Game() : mWindow(sf::VideoMode(640, 480), "Game")
{
    mEvent = EventFactory::Get()->CreateEvent("Field");
    mEvent->prepareEvent(ObjectManager::Get()->getAllObjects());
}

void Game::run()
{
    auto TimePerFrame = sf::seconds(1.f / 60.f);
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    while (mWindow.isOpen())
    {
        mEvent->processEvent(mWindow);
        timeSinceLastUpdate += clock.restart();
        while (timeSinceLastUpdate > TimePerFrame)
        {
            timeSinceLastUpdate -= TimePerFrame;
            mEvent->processEvent(mWindow);
            update(TimePerFrame);
        }
        mEvent->render(mWindow);
    }
}

void Game::update(sf::Time deltaTime)
{
    mEvent->update(deltaTime);
    auto newEvent = mEvent->isEventEnd();
    if (newEvent != nullptr) {
        mEvent = std::move(newEvent);
    }
}