#ifndef RULES

#define RULES

#include "Object.h"

struct Rule{
    static void attack(std::shared_ptr<Object> from, std::shared_ptr<Object> to);
    static void move(std::shared_ptr<Object> object, sf::Time deltaTime);
    static void pickupItem(std::shared_ptr<Object> unit, std::shared_ptr<Object> item);
    static int nextLevel(int curLevel);
    static void levelUpCheck(std::shared_ptr<Object> unit);
    static int xpFromUnit(std::shared_ptr<Object> unit);
    static void printStats();
};


#endif