#ifndef GAME

#define GAME

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "ObjectManager.h"
#include "Event.h"

class Event;

const int PLAYER = 0;

class Game{
    std::unique_ptr<Event> mEvent;

    sf::RenderWindow mWindow;

public:
    Game();
    void run();

private:
    void processEvent();
    void update(sf::Time deltaTime);
    void render();
};

#endif