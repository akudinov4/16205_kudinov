#include "Object.h"

Object::Object(){
    std::cout << "Creating Object class" << std::endl << "Name: ";
    //std::cin >> name;
    file >> name;
    std::cout << name;
    std::cout << std::endl << "Object Type: ";
    std::string type;
    file >> type;
    std::cout << type << std::endl;
    //std::cin >> type;
    if (type == "Player") addProperty(std::shared_ptr<Player>(new Player));
    if (type == "Enemy") addProperty(std::shared_ptr<Enemy>(new Enemy));
    if (type == "Item") addProperty(std::shared_ptr<Item>(new Item));
    if (type == "NPC") addProperty(std::shared_ptr<NPC>(new NPC));
}

Object::Object(Info& info){

}

Object::~Object(){
    
}

void Info::openFile(std::string filename){
    file.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    file.open(filename);
    std::cout << "File opened" << std::endl;
}

std::shared_ptr<BattleStats> Info::createBattleStats(){
    auto res = std::shared_ptr<BattleStats>(new BattleStats);
    /*std::cout << "Creating BattleStats class" << std::endl << "Health: ";
    std::cin >> res->maxHealth;
    std::cout << "Damage: ";
    std::cin >> res->damage;
    std::cout << "Armor: ";
    std::cin >> res->armor;*/

    file >> res->maxHealth;
    std::cout << "Creating BattleStats class" << std::endl << "Health: " << res->maxHealth << std::endl;
    file >> res->baseDamage;
    std::cout << "Damage: " << res->baseDamage << std::endl;
    file >> res->armor;
    std::cout << "Armor: " << res->armor << std::endl;

    res->curHealth = res->maxHealth;
    return res;
}

std::shared_ptr<ItemStats> Info::createItemStats(){
    auto res = std::shared_ptr<ItemStats>(new ItemStats);
    std::cout << "Creating ItemStats class" << std::endl << "Health: ";
    std::cin >> res->health;
    std::cout << "Mana: ";
    std::cin >> res->mana;
    std::cout << "Damage: ";
    std::cin >> res->damage;
    std::cout << "Armor: ";
    std::cin >> res->armor;
    std::cout << "Move Speed: ";
    std::cin >> res->moveSpeed;
    return res;
}