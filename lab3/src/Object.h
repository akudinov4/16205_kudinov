#ifndef OBJECT

#define OBJECT

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <string>
#include <map>
#include <typeinfo>
#include <memory>
#include <iostream>
#include <fstream>
#include <vector>

#include "Events/ObjectsProp.h"


static std::ifstream file;

struct ItemStats;
struct Player{};
struct Item{};
struct NPC{};

struct Enemy{};

struct Info{
    static void openFile(std::string name);
    static std::shared_ptr<BattleStats> createBattleStats();
    static std::shared_ptr<ItemStats> createItemStats();
};

class Object {
    std::string name;
    std::map <std::string, std::shared_ptr<void>> mProperties;

public:

    Object();
    ~Object();

    Object(Info& info);

    template<class T> void addProperty(std::shared_ptr<T> className) {
        mProperties.insert(std::make_pair(typeid(T).name(), className));
    };
    template<class T> std::shared_ptr<T> getProperty (){
        auto res = mProperties.find(typeid(T).name());
        if(res == mProperties.end()) return nullptr;
        return std::static_pointer_cast<T>(res->second);
    }; 
    std::string getName() {return name;}
};

struct Render{
    sf::RectangleShape object;
};

struct ItemStats{
    int health;
    int mana;
    int damage;
    int armor;
    int moveSpeed;
};

/*
class Buffs{
    std::map <std::string, Buff*> mBuffs;

public:
    Buffs();
    ~Buffs();

    void addBuff(std::string buffName);
    void removeBuff(std::string buffName);
};
*/
struct Inventory {
    std::vector <std::shared_ptr<Object>> mInventory;
};


#endif