#ifndef EVENTFACTORY

#define EVENTFACTORY


#include "Event.h"

#include <functional>

class EventFactory{   
    EventFactory() = default;
    typedef std::map<std::string, std::function<std::unique_ptr<Event>()>> FactoryMap;
    FactoryMap m_EventFactory;

public:
    ~EventFactory() = default;

    static EventFactory* Get(){
        static EventFactory instance;
        return &instance;
    }

    int Register(const std::string &EventName, std::function<std::unique_ptr<Event>()> CreateEvent);
    std::unique_ptr<Event> CreateEvent(const std::string name);
};

#endif