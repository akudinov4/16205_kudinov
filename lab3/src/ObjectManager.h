#ifndef OBJMANAGER

#define OBJMANAGER

#include "Object.h"

class ObjectManager{
    std::map<int, std::shared_ptr<Object>> mObjects;

    ObjectManager();
public:
    ~ObjectManager() = default;
    
    static ObjectManager* Get(){
        static ObjectManager instance;
        return &instance;
    };
    
    void initObjects();
    void addObject(int ID, std::shared_ptr<Object> object);
    std::shared_ptr<Object> getObject(int ID);
    std::map<int, std::shared_ptr<Object>>& getAllObjects();
    
};

#endif