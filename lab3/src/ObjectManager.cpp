#include "ObjectManager.h"

ObjectManager::ObjectManager(){
    initObjects();
}

void ObjectManager::addObject(int ID, std::shared_ptr<Object> object){
    mObjects.insert(std::make_pair(ID, object));
}

std::shared_ptr<Object> ObjectManager::getObject(int ID){
    auto res = mObjects.find(ID);
    if(res == mObjects.end()) return nullptr;
    return res->second;
}

std::map<int, std::shared_ptr<Object>>& ObjectManager::getAllObjects(){
    return mObjects;
}

void ObjectManager::initObjects()
{
    for (int i = 0; i < 6; i++)
    {
        auto mObject = std::shared_ptr<Object>(new Object);
        mObject->addProperty(Info::createBattleStats());
        mObject->addProperty(std::shared_ptr<FieldStats>(new FieldStats));
        mObject->addProperty(std::shared_ptr<Render>(new Render));
        mObject->addProperty(std::shared_ptr<BaseStats>(new BaseStats));
        mObject->addProperty(std::shared_ptr<DialogRender>(new DialogRender));
        mObject->addProperty(std::shared_ptr<Speech>(new Speech));
        mObject->getProperty<Render>()->object.setSize(sf::Vector2f(20.f, 40.f));
        float x = rand() % 600 + 20;
        float y = rand() % 260 + 40;
        mObject->getProperty<Render>()->object.setPosition(sf::Vector2f(x, y));
        if (mObject->getProperty<Player>() != nullptr)
        {
            mObject->getProperty<Render>()->object.setFillColor(sf::Color::Yellow);
        }
        else if (mObject->getProperty<Enemy>() != nullptr)
        {
            mObject->getProperty<BaseStats>()->level = 10;
            mObject->getProperty<Render>()->object.setFillColor(sf::Color::White);
        } else {
            mObject->getProperty<Render>()->object.setFillColor(sf::Color::Green);
        }
        addObject(i, mObject);
    }
}