#include "Textures.h"

void TextureHolder::loadTexture(int ID, const std::string &filename){
    std::unique_ptr<sf::Texture> texture(new sf::Texture);
    texture->loadFromFile(filename);
    mTextures.insert(std::make_pair(ID, std::move(texture)));
}

void TextureHolder::clear(){
    mTextures.clear();
}