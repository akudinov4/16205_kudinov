#ifndef EVENT

#define EVENT

#include "Object.h"
#include <functional>

class Event
{
protected:
  std::unique_ptr<Event> mSubEvent;
  bool mIsSubEvent = false;
public:
  virtual void prepareEvent(std::map<int, std::shared_ptr<Object>> &) = 0;
  virtual void startEvent() = 0;
  virtual void render(sf::RenderWindow &) = 0;
  virtual void processEvent(sf::RenderWindow &) = 0;
  virtual void update(sf::Time deltaTime) = 0;
  virtual std::unique_ptr<Event> isEventEnd() = 0;
};

#endif