#include "EventFactory.h"

int EventFactory::Register(const std::string &eventName, std::function<std::unique_ptr<Event>()> CreateEvent)
{
    m_EventFactory.insert(std::make_pair(eventName, std::move(CreateEvent)));
    return 0;
}

std::unique_ptr<Event> EventFactory::CreateEvent(const std::string name)
{
    FactoryMap::iterator it = m_EventFactory.find(name);
    if (it != m_EventFactory.end())
        return it->second();
    return nullptr;
}