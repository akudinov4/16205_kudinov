#include "Rules.h"

void Rule::attack(std::shared_ptr<Object> from, std::shared_ptr<Object> to){
    auto baseDamage = from->getProperty<BattleStats>()->baseDamage;
    auto damageFromStr = from->getProperty<BaseStats>()->strength;
    auto damage = baseDamage + damageFromStr;

    auto armor = to->getProperty<BattleStats>()->armor;
    
    damage = damage * damage / (armor + damage);
    to->getProperty<BattleStats>()->curHealth -= damage;
    if(to->getProperty<BattleStats>()->curHealth < 0)
        to->getProperty<BattleStats>()->curHealth = 0; 
    std::string strHP = std::to_string(to->getProperty<BattleStats>()->curHealth) + "/" + std::to_string(to->getProperty<BattleStats>()->maxHealth);
    to->getProperty<BattleRender>()->HP.setString(strHP);
}
 
void Rule::move(std::shared_ptr<Object> object, sf::Time deltaTime){
    auto moveSpeed = object->getProperty<FieldStats>()->moveSpeed;
    auto stats = object->getProperty<FieldStats>();
    sf::Vector2f movement(0.f, 0.f);
    if(stats->mIsMovingUp) movement.y -= moveSpeed;
    if(stats->mIsMovingDown) movement.y += moveSpeed;
    if(stats->mIsMovingRight) movement.x += moveSpeed;
    if(stats->mIsMovingLeft) movement.x -= moveSpeed;
    auto &objectR = object->getProperty<Render>()->object;
    objectR.move(movement * deltaTime.asSeconds());
    if(objectR.getPosition().x < 0) objectR.setPosition(640, objectR.getPosition().y);
    if(objectR.getPosition().x > 640) objectR.setPosition(0, objectR.getPosition().y);
    if(objectR.getPosition().y < 0) objectR.setPosition(objectR.getPosition().x, 480);
    if(objectR.getPosition().y > 480) objectR.setPosition(objectR.getPosition().x, 0);
}

void Rule::pickupItem(std::shared_ptr<Object> unit, std::shared_ptr<Object> item){
    if(unit->getProperty<FieldStats>()->mAction){
        if(unit->getProperty<Inventory>() == nullptr) return;
        unit->getProperty<Inventory>()->mInventory.push_back(item);
    }
}

int Rule::nextLevel(int curLevel){
    return ((4 * curLevel * curLevel * curLevel) / 5) + 1;
}

void Rule::levelUpCheck(std::shared_ptr<Object> unit){
    auto& xp = unit->getProperty<BaseStats>()->xp;
    auto& level = unit->getProperty<BaseStats>()->level;
    auto baseStats = unit->getProperty<BaseStats>();
    int addXP = 0;
    auto xpForNextLevel = nextLevel(level);
    while (xp >= nextLevel(level)) {
        while (xp >= 2*nextLevel(level)) {addXP += nextLevel(level); xp -= nextLevel(level);} 
        xp = xp % nextLevel(level);
        xp += addXP;
        level++;
        baseStats->intelligence += 10;
        baseStats->strength += 10;
        baseStats->speed += 10;
    }
}

int Rule::xpFromUnit(std::shared_ptr<Object> unit){
    auto level = unit->getProperty<BaseStats>()->level;
    return ((level * level) / 5) + 1;
}

void Rule::printStats(){
    std::cout << std::endl << "Hero stats" << std::endl;
    auto hero = ObjectManager::Get()->getObject(0);
    auto res = hero->getProperty<BattleStats>();
    std::cout << "Health: " << res->curHealth << std::endl;
    std::cout << "Damage: " << res->baseDamage << std::endl;
    std::cout << "Armor: " << res->armor << std::endl;
    auto res1 = hero->getProperty<BaseStats>();
    std::cout << "Level: " << res1->level << std::endl;
    std::cout << "Strength: " << res1->strength << std::endl;


}